/*zasieg_zmiennych01.c*/
/*Program prosi użytkownika o podanie temperatury
  w stopniach Celsjusza.
  Następnie przelicza podaną temperaturę
  na stopnie Fahrenheita i wyświetla wynik.

  Opisano zasięg zmiennych wykorzystywanych w programie.*/

#include<stdio.h>

float cels2fahr(float);
float readCels();
void printResult(float, float);

int main(){
    //zasięg zmiennej celsjusz ograniczony jest do funkcji main()
    float celsjusz;
    celsjusz = readCels();
    printResult(celsjusz, cels2fahr(celsjusz));
    return 0;
}

float cels2fahr(float c){
    //zmienna automatyczna c
    //zasięg zmiennej jest ograniczony do funkcji cels2fahr()
    return 32 + 1.8*c;
}

float readCels(){
    //zmienna lokalna c
    //zasięg zmiennej jest ograniczony do funkcji readCels()
    //jest to inna zmienna niż w funkcji cels2fahr()
    float c;
    printf("Podaj temperature w Celsjuszach: ");
    scanf("%f",&c);
    return c;
}

void printResult(float c, float f){
    //zmienne automatyczne c oraz f
    //zasięg zmiennych jest ograniczony do funkcji cprintResults()
    printf("Temperatura %.2f stopni Celsjusza\n",c);
    printf("odpowiada temperaturze %.2f stopni Fahrenheita.\n", f);
    return;
}



