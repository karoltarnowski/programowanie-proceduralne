/*zasieg_zmiennych02.c*/
/*Program prosi użytkownika o podanie temperatury
  w stopniach Celsjusza.
  Następnie przelicza podaną temperaturę
  na stopnie Fahrenheita i wyświetla wynik.

  Program wykorzystuje zmienne globalne.*/

#include<stdio.h>

//deklaracja zmiennych globalnych
//umieszczona przed funkcją main()
float celsjusz;
float fahrenheit;

//wszystkie funkcje mają puste listy argumentów
//oraz są typu void - nie zwracają wyników
void cels2fahr();
void readCels();
void printResult();

int main(){
    readCels();
    cels2fahr();
    printResult();
    return 0;
}

void cels2fahr(){
    //obie zmienne są dostępne w funkcji cels2fahr()
    //dzieki deklaracji globalnej
    //funkcja może odczytać wartość zmiennej celsjusz
    //oraz zapisać wynik w zmiennej fahrenheit
    fahrenheit = 32 + 1.8*celsjusz;
}

void readCels(){
    //funkcja readCels() wykorzystuje zmienną globalną celsjusz
    printf("Podaj temperature w Celsjuszach: ");
    scanf("%f",&celsjusz);
}

void printResult(){
    //funkcja printResuls() wypisuje wartości
    //zapisane w zmiennych globalnych celsjusz oraz fahrenheit
    printf("Temperatura %.2f stopni Celsjusza\n",celsjusz);
    printf("odpowiada temperaturze %.2f stopni Fahrenheita.\n", fahrenheit);
}



