/*otwarcie_nieistniejacego_pliku.c*/
/*Dostęp do pliku.*/
#include <stdio.h>

int main()
{
    FILE* fptr;
    fptr = fopen("danetxt","r");
    //jeśli plik danetxt nie istnieje,
    //nie można otworzyć go do odczytu
    //fopen zwróci NULL
    printf("%p",fptr);
    fclose(fptr);
    return 0;
}
