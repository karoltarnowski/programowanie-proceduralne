/*odczyt_z_pliku.c*/
/*Odczyt danych z pliku.*/
#include <stdio.h>

int main()
{
    //deklaracja zmiennych
    //i wskaźnika do pliku
    double a, b, c;
    FILE* fptr;

    //otworzenie pliku do odczytu
    fptr = fopen("abc.txt","r");
    //jeśli otwarto plik poprawnie
    if( fptr != NULL ){
        //wczytaj dane
        fscanf(fptr,"a = %lf\n",&a);
        fscanf(fptr,"b = %lf\n",&b);
        fscanf(fptr,"c = %lf\n",&c);
        fclose(fptr);
    }

    //wypisz dane na ekran
    printf("a = %lf\n",a);
    printf("b = %lf\n",b);
    printf("c = %lf\n",c);

    return 0;
}




