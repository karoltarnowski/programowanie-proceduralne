/*strumienie.c*/
/*Wykorzystanie stdin/stdout.*/
#include <stdio.h>

int main()
{
    double a, b;
    FILE* fptr;

    //otworzenie pliku do odczytu
    fptr = fopen("abcd.txt","r");
    //jesli nie udalo sie otworzyc pliku
    if( fptr == NULL )
        fptr = stdin; //ustaw wskaznik na stdin

    //wczytaj dane
    fscanf(fptr,"a = %lf\n",&a);
    fscanf(fptr,"b = %lf",&b);

    //wypisz dane na ekran
    fprintf(stdout,"a = %lf\n",a);
    fprintf(stdout,"b = %lf\n",b);

    fclose(fptr);

    return 0;
}



