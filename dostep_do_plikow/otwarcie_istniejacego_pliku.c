/*otwarcie_istniejacego_pliku.c*/
/*Dostęp do pliku.*/
#include <stdio.h>

int main()
{
    FILE* fptr;
    fptr = fopen("dane.txt","r");
    //jeśli plik dane.txt istnieje,
    //zostnie otwarty do odczytu
    //fopen zwróci niezerowy wskaźnik
    printf("%p",fptr);
    fclose(fptr);
    return 0;
}
