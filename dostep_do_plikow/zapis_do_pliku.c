/*zapis_do_pliku.c*/
/*Zapis danych do pliku.*/
#include <stdio.h>

int main()
{
    //deklaracja trzech zmiennych
    double a, b, c;
    //i wskaźnika do pliku
    FILE* fptr;

    //wczytanie wartości zmiennych
    //ze standardowego wejścia
    printf("Podaj a: ");
    scanf("%lf",&a);
    printf("Podaj b: ");
    scanf("%lf",&b);
    printf("Podaj c: ");
    scanf("%lf",&c);

    //otworzenie pliku do zapisu
    fptr = fopen("abc.txt","w");
    //zapis danych sformatowanych do pliku
    //(tylko jesli plik zostal poprawnie otwarty)
    if( fptr != NULL ){
        fprintf(fptr,"a = %lf\n",a);
        fprintf(fptr,"b = %lf\n",b);
        fprintf(fptr,"c = %lf\n",c);
        fclose(fptr);
    }
    return 0;
}










