/*przez_adres.c*/
/*Przekazywanie zmiennej
  przez adres (wskaźnik).*/

#include <stdio.h>

void half(int * pa);

int main(){
    //deklaracja i inicjalizacja zmiennej a
    int a = 13;

    //wyświetlenie zmiennej a
    //zmienna a = 13
    printf("(main) a = %d\n",a);

    //zmienna a przekazana do funkcji half() przez wskaźnik
    //do funkcji() half przekazywany jest adres zmiennej
    //adres zmiennej odczytywany jest operatorem adresu &
    half(&a);

    //ponowne wyswietlenie zmiennej a
    //ponieważ funkcja half działała na oryginale
    //czyli a zmieniła wartość
    //a = 6
    printf("(main) a = %d\n",a);

    return 0;
}

void half(int * pa){
    //automatyczna zmienna lokalna pa
    //jest wskaźnikiem na int
    //jako argument przekazano adres a,
    //zatem pa wskazuje na oryginalną zmienną a

    //zmienna pod adresem pa jest dzielona przez 2
    //wykorzystano operator wskaźnikowy *
    //aby uzyskać dostęp do zmiennej a
    *pa = *pa/2;

    //wyswietlenie zmiennej wskazywanej przez pa (czyli a)
    printf("(half) a = %d\n",*pa);
}























