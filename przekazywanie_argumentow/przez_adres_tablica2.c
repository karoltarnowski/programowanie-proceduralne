/*przez_adres_tablica2.c*/
/*Przekazywanie tablicy do funkcji.
Równoważna deklaracja funkcji
z wykorzystaniem wskaźnika.*/

#include <stdio.h>

//float * zamiast float []
void zeruj(float *, int);

int main(){
   int i,n = 5;
   float tab[5] = {2.3,5.7,11.13,17.19,23.27};
   for(i=0;i<n;i++)
      printf("tab[%d] = %g\n",i,tab[i]);
   zeruj(tab,n);
   for(i=0;i<n;i++)
      printf("tab[%d] = %g\n",i,tab[i]);
   return 0;
}

//float * zamiast float []
void zeruj(float * t, int n){
   int i;
   for(i=0;i<n;i++)
      t[i] = 0;
}
