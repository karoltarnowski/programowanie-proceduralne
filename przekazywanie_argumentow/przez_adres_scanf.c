/*przez_adres_scanf.c*/
/*Przekazywanie zmiennej
  przez adres - funkcja scanf().*/

#include <stdio.h>

int main(){
   int a;
   printf("Podaj liczbe calkowita: ");

   //zmienna przekazana jest do funkcji scanf()
   //przez adres
   scanf("%d",&a);

   //wyswietlenie zmiennej a
   printf("a = %d\n",a);

   return 0;
}
