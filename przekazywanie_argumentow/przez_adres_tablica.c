/*przez_adres_tablica.c*/
/*Przekazywanie tablicy do funkcji*/

#include <stdio.h>

//funkcja zeruj przypisuje wszystkim elementom
//tablicy wartość 0
void zeruj(float [], int);


int main(){
   int i,n = 5;
   //deklaracja i inicjalizacja tablicy tab
   float tab[5] = {2.3,5.7,11.13,17.19,23.27};

   //wyswietlenie tablicy
   for(i=0;i<n;i++)
      printf("tab[%d] = %g\n",i,tab[i]);

   //wywolanie funkcji zeruj()
   //tablica przekazana do funkcji
   zeruj(tab,n);

   //ponowne wyswietlenie tablicy
   for(i=0;i<n;i++)
      printf("tab[%d] = %g\n",i,tab[i]);
   return 0;
}

//funkcja zeruj przypisuje n elementom
//tablicy t wartość 0
void zeruj(float t[], int n){
   int i;
   //indeks i przyjmuje wartości od 0 do n-1
   for(i=0;i<n;i++)
      t[i] = 0; //wszystkim elementom t[i] przypisuje wartość 0
}

























