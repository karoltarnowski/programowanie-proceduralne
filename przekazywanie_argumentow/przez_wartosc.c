/*przez_wartosc.c*/
/*Program ilustruje przekazywanie zmiennej
  przez wartość (przez kopię).*/

#include <stdio.h>

void half(int a);

int main(){
    //deklaracja i inicjalizacja zmiennej a
    int a = 13;
    //wyświetlenie zmiennej a
    //zmienna a = 13
    printf("(main) a = %d\n",a);
    //zmienna a przekazana do funkcji half() przez wartość
    //funkcja half() wykonuje działania określone w definicji
    half(a);
    //ponowne wyswietlenie zmiennej a
    //a = 13
    //ponieważ funkcja half() działała na kopii
    //czyli a nie zmieniła wartości
    printf("(main) a = %d\n",a);
    return 0;
}

void half(int a){
    //automatyczna zmienna lokalna a
    //przyjmuje wartość 13 - kopia wartości a z funkcji main()
    //zmienna a podzielona przez 2
    a = a/2;
    //wyswietlenie zmiennej a = 6
    printf("(half) a = %d\n",a);
}


















