/*znajdowanie_maksimum.c*/
/*Program demonstruje działanie
funkcji max(), która w tablicy
liczb rzeczywistych (n-elementowej)
znajduje największy element.*/

#include<stdio.h>

//deklaracja funkcji max()
float max(float t[], int n);

#define N 10
//kod źrodłowy funkcji głównej
int main()
{
    float t[N] = {0.284, 0.091, 0.429, 0.547, 0.983,\
                  0.676, 0.827, 0.763, 0.714, 0.136};
    printf("max: %.3f",max(t,N));
    return 0;
}

//kod źródłowy funkcji max()
float max(float t[], int n){
	float m = t[0];
	int i;
	for( i=1; i<n; i++){
		if( t[i] > m )
			m = t[i];
	}
	return m;
}
