/*max.c*/
//kod źródłowy funkcji max()
float max(float t[], int n){
	float m = t[0];
	int i;
	for( i=1; i<n; i++){
		if( t[i] > m )
			m = t[i];
	}
	return m;
}
