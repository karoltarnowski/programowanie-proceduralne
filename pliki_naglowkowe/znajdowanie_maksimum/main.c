/*main.c*/
/*Przykładowy projekt pokazujący
rozbicie kodu źródłowego programu
na kilka plików:
- main.c (funkcja główna programu),
- sort.h (plik nagłówkowy),
- max.c  (plik zawierający kod źródłowy funkcji max()).*/

#include <stdio.h>
#include "sort.h"   //polecenie preprocesora, które dołącza plik nagłówkowy

#define N 10
int main()
{
   float t[N] = {0.284, 0.091, 0.429, 0.547, 0.983,\
                 0.676, 0.827, 0.763, 0.714, 0.136};
   printf("max: %.3f",max(t,N));
   return 0;
}

