/*Program projektu debugowanie01 po wprowadzeniu poprawki.*/

#include <stdio.h>

double obliczOdsetki(double podstawa, double oprocentowanie, int lata);

int main(){
    double podstawa;
    double oprocentowanie;
    int lata;
    printf("Podaj podstawe: ");
    scanf("%lf", &podstawa);
    printf("Podaj oprocentowanie: ");
    scanf("%lf", &oprocentowanie);
    printf("Podaj liczbe lat: ");
    scanf("%d", &lata);
    printf("Po %d latach bedziesz miec %.2f PLN.\n", lata,
           obliczOdsetki(podstawa, oprocentowanie, lata ));
    return 0;
}

double obliczOdsetki(double podstawa, double oprocentowanie, int lata){
    int i;
    double koncowy_mnoznik;
    for(i=0; i<lata; i++){
        koncowy_mnoznik *= (1 + oprocentowanie);
    }
    return podstawa * koncowy_mnoznik;
}















