/*argumenty_linii_polecen*/
/*Program wypisuje zawartość plików
wymienionych w linii poleceń na stdout.
Zawiera funkcje do kopiowania plików.
Ilustruje wykorzystanie argumentów
linii poleceń*/

#include<stdio.h>

void filecopy(FILE*, FILE*);

int main(int argc, char** argv)
{
    //deklaracja wskaźnika do pliku
    FILE *fp;

    //jeżeli jest jeden argumbent linii poleceń
    //(nazwa programu)
    if(argc == 1)
    {
        //kopiowane jest standardowe wejście
        filecopy(stdin, stdout);
    }

    //jeśli są jakieś argumenty
    else
        //przejdź po wszystkich argumentach
        while(--argc > 0)
        {
            //otworz plik (nazwa pliku jest wskazywana przez kolejny element
            //w tablicy wskaźników argv)
            //w razie niepowodzenia
            if( (fp = fopen(*++argv, "r") ) == NULL )
            {
                //wyświetl komunikat i zakończ program
                printf(" nie mozna otworzyc %s\n", *argv);
                return 1;
            }
            //jeśli udało się otworzyć
            else
            {
                //skopiuj plik fp na stdout
                filecopy(fp, stdout);
                //zamknij plik
                fclose(fp);
            }
        }
    return 0;
}

//funkcja kopiujaca plik ifp (input)
//do pliku ofp (output)
void filecopy(FILE *ifp, FILE *ofp)
{
    int c;
    //dopoki ( (znak wczytany z wejscia) jest rozny od EOF )
    //wypisuj ten znak na wyjscie
    while( (c = getc(ifp)) != EOF )
        putc(c,ofp);
}

















