#include <stdio.h>
#include <stdlib.h>

struct Node{
    int v;
    struct Node* next;
};

typedef struct Node Node;

void printList(const Node*);

int main()
{
    Node* head;
    head = malloc(sizeof(Node));
    head->v = 10;
    head->next = malloc(sizeof(Node));
    head->next->v = 11;
    printList(head);
    return 0;
}

void printList(const Node* n){
    if(n != NULL){
        printf("%d\n",n->v);
        printList(n->next);
    }
}






