/*rekurencja.c*/
/*Przykład definicji rekurencyjnej funkcji.
Funkcja silnia(n) oblicza wartość silni
dla liczby całkowitej dodatniej n.*/

#include <stdio.h>

int silnia(int n);

int main(){
    int n = 5;
    printf("%d! = %d\n",n,silnia(n));
    return 0;
}

//defincja funkcji obliczającej silnię rekurencyjnie
//funkcja silnia() wywołuję funkcję silnia()
//dla argumentu zmniejszonego o 1
int silnia(int n){
    //istotne jest umieszczenie w funkcji warunku zatrzymania rekurencji
    if(n==0)
        return 1;
    else
        return n*silnia(n-1);
}
//poprawność działania tej implementacji
//jest ograniczona do małych liczb całkowitych
//dodatnich
