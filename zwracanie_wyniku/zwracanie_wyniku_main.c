/*zwracanie_wyniku_main.c*/
/*Zwracanie wyniku przez funkcję main()*/

#include <stdio.h>

float max(float, float, float);

//funkcja main() jest typu int
int main(){
   float a, b, c;
   a = 3.14; //liczba pi
   b = 2.72; //podstawa logarytmu naturalnego
   c = 4.67; //stala Feigenbauma
   printf("Najwieksza z liczb = %g\n",max(a,b,c));
   return 0; //prawidlowe zakonczenie dzialania programu
}

float max(float x, float y, float z){
   if(y>x)
      x = y;
   if(z>x)
      x = z;
   return x;
}
