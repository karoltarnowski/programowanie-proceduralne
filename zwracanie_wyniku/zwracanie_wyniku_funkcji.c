/*zwracanie_wyniku_funkcji.c*/
/*Zwracanie wyniku przez funkcję*/

#include <stdio.h>

//typ funkcji określa typ zwracanego przez nią wyniku
float max(float, float, float);

int main(){
   float a, b, c;
   a = 3.14; //liczba pi
   b = 2.72; //podstawa logarytmu naturalnego
   c = 4.67; //stala Feigenbauma
   //wywołanie funkcji max() zwraca wynik typu float
   printf("Najwieksza z liczb = %g\n",max(a,b,c));
   return 0;
}

//funkcja max() zwraca największy z trzech argumentów
//argumenty są typu float, zatem wynik również
float max(float x, float y, float z){
   if(y>x)
      x = y;
   if(z>x)
      x = z;
   return x; //zwracana jest wartosc zmiennej typu float
}
