/*point.h*/
/*Plik nagłówkowy zawierający
definicję struktury point
oraz deklaracje obsługujących ją
funkcji.*/
#ifndef POINT_H_INCLUDED
#define POINT_H_INCLUDED

struct point {
   double x;
   double y;
};

void readPoint(struct point*);
void printPoint(struct point*);

#endif // POINT_H_INCLUDED
