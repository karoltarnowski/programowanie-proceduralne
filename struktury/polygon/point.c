/*point.c*/
/*Plik źródłowy zawierający
definicje funkcji obsługujących
strukturę point.*/

#include<stdio.h>
#include"point.h"

//funkcja printPoint()
//pobiera wskaznik na punkt
//wyswietla wspolrzedne na standardowe wyjscie
//nic nie zwraca
void printPoint(struct point * p)
{
    //sprawdznie, czy przekazany wskaznik jest niepusty
    if(p!=NULL)
        printf("[%f, %f]",p->x, p->y);
}

//funkcja readPoint()
//pobiera wskaznik na punkt
//wczytuje wspolrzedne ze standardowego wejscia
//nic nie zwraca
void readPoint(struct point * p)
{
    //sprawdznie, czy przekazany wskaznik jest niepusty
    if(p!=NULL)
    {
        printf("x: ");
        scanf("%lf", &(p->x));
        printf("y: ");
        scanf("%lf", &(p->y));
    }
}
