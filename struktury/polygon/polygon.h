/*polygon.h*/
/*Plik nagłówkowy zawierający
definicję struktury polygon
oraz deklaracje obsługujących ją
funkcji.*/

#ifndef POLYGON_H_INCLUDED
#define POLYGON_H_INCLUDED

//dołączamy plik nagłówkowy
//struktury point
//ponieważ point pojawia się
//jako pole składowe struktury
#include"point.h"

//struktura zawiera dwa pola
struct polygon
{
    int n;            //liczba wierzchołków
    struct point * v; //wskaźnik na tworzoną dynamicznie tablicę wierzchołków
};

//deklaracje funkcji obsługujących wielokąt
struct polygon * makePolygon(int);
void readPolygon(struct polygon * p);
void printPolygon(struct polygon * p);
void deletePolygon(struct polygon ** p);

#endif // POLYGON_H_INCLUDED
