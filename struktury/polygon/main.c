/*main.c*/
/*Program demonstruje wykorzystanie
struktury polygon zdefiniowanej
w pliku nagłówkowym polygon.h
wraz z funkcjami zdefiniowanymi
w pliku polygon.c.*/

//plik nagłówkowy polygon.h
//zawiera definicję struktury
//i deklaracje funkcji ją obsługujących
#include "polygon.h"

//program demonstruje operacje na strukturze
int main()
{
    //deklaracja wskaźnika na wielokąt
    //połączona z inicjalizacją
    //makePolygon tworzy strukturę
    //reprezentującą wielokąt o n kątach
    struct polygon * p = makePolygon(5); //tworzymy pięciokąt

    //odczytanie współrzędnych wierzchołków
    //wielokąta ze standardowego wejścia
    readPolygon(p);

    //wypisanie współrzędnych wierzchołków
    //wielokąta na standardowe wyjście
    printPolygon(p);

    //zwolnienie pamięci zajmowanej
    //przez strukturę danych
    //wskaźnik p przekazany przez wskaźnik
    //po usunięciu struktury wskaźnik ustawiony na NULL
    deletePolygon(&p);

    //wywołana funkcja wypisująca współrzędne wierzchołków
    //wielokąta na standardowe wyjście
    printPolygon(p);  //nic sie nie stanie, wskaźnik p jest wyzerowany

    readPolygon(p);   //podobnie z funkcją odczytująca współrzędne

    return 0;
}
