/*polygon.c*/
/*Plik źródłowy zawierający
definicje funkcji obsługujących
strukturę polygon.*/

#include<stdlib.h>   //obsluga pamieci: malloc(), free(), NULL
#include<stdio.h>    //wypisywanie informacji: printf()
#include "polygon.h" //definicja struktury

//funkcja makePolygon()
//pobiera liczbę wierzchołków
//zwraca wskaźnik na utworzoną strukturę
struct polygon * makePolygon(int n)
{
    //funkcja działa tylko wtedy gdy n jest dodatnie
    if(n>0)
    {
        struct polygon * p;                 //wskaźnik wykorzystywany lokalnie
        p = malloc(sizeof(struct polygon)); //alokacja pamięci na strukturę

        //ustawienie pola n na liczbę wierzchołków
        p->n = n; //(wykorzystany operator ->)

        //alokacja pamięci na tablicę wierzchołków
        p->v = malloc(n*sizeof(struct point));

        return p;   //zwracany jest wskaźnik na utworzoną strukturę
    }
    //w p.p. zwraca wyzerowany wskaźnik
    else
        return NULL;
};

//funkcja readPolygon()
//pobiera wskaźnik na wielokąt
//wczytuje wierzchołki ze standardowego wejścia
//nic nie zwraca
void readPolygon(struct polygon * p)
{
    //sprawdzenie, czy przekazany wskaźnik jest niepusty
    if(p!=NULL)
    {
        int i;
        //pętla po wszystkich wierzchołkach
        for(i=0; i< p->n; i++)
        {
            printf("vertice %d:\n",i);
            readPoint(&(p->v[i]));     //wczytanie i-tego wierzchołka
        }
    }
}

//funkcja printPolygon()
//pobiera wskaźnik na wielokąt
//wyświetla wierzchołki na standardowe wyjście
//nic nie zwraca
void printPolygon(struct polygon * p)
{
    //sprawdznie, czy przekazany wskaźnik jest niepusty
    if( p!= NULL)
    {
        int i;
        //pętla po wszystkich wierzchołkach
        for(i=0; i< p->n; i++)
        {
            printf("vertice %d:\n",i);
            printPoint(&(p->v[i]));     //wypisanie i-tego wierzchołka
            printf("\n");
        }
    }
}

//funkcja makePolygon()
//pobiera wskaźnik na utworzoną strukturę (przez referencję)
//pobiera zatem wskaźnik na wskaźnik
void deletePolygon(struct polygon ** p)
{
    //sprawdznie, czy przekazany wskaźnik jest niepusty
    if(*p!=NULL){
        free((*p)->v); //zwolnienie tablicy wierzchołków
        free(*p);      //usunięcie struktury
        *p = NULL;     //wyzerowanie wskaźnika
    }
}
