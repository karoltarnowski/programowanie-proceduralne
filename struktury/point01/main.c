/*main.c*/
/*Program demonstruje wykorzystanie
struktury point zdefiniowanej
w pliku nagłówkowym point.h.*/

#include <stdio.h>
#include "point.h" //dołączenie pliku nagłówkowego
//nazwa pliku umieszczona w cudzysłowie

int main()
{
    //deklaracja zmiennych a, b
    //będących strukturami
    struct point a, b;

    //przypisanie wartości polom struktury a ...
    a.x =  1.1;
    a.y = -2.0;

    //... oraz b
    b.x = 0;
    b.y = 0;

    //wyświetlenie wartości pól struktury
    printf("a.x = %lf\n", a.x);
    printf("a.y = %lf\n", a.y);
    printf("b.x = %lf\n", b.x);
    printf("b.y = %lf\n", b.y);

    return 0;
}
