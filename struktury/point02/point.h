/*point.h*/
/*Plik nagłówkowy zawierający
definicję struktury point
oraz deklaracje obsługujących ją
funkcji.*/
#ifndef POINT_H_INCLUDED
#define POINT_H_INCLUDED

struct point {
   double x;
   double y;
};

//w pliku nagłówkowym dodano deklaracje
//dwóch funkcji obsługujących strukturę point:
    //funkcja, która wczytuje współrzędne od użytkownika
void readPoint(struct point*);
    //funkcja, która wyświetla współrzędne na ekran
void printPoint(struct point*);
//obie funkcje pobierają argument przez wskaźnik

#endif // POINT_H_INCLUDED
