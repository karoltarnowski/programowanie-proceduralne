/*point.c*/
/*Plik źródłowy zawierający
definicje funkcji obsługujących
strukturę point.*/

//wykorzystanie funkcji printf(), scanf()
//wymaga dołączenia pliku stdio.h
#include<stdio.h>
//dołączamy plik nagłówkowy,
//kompilując funkcje pracujące na strukturze
//musimy znać jej definicję
#include"point.h"

void readPoint(struct point* p){
    printf("x coordinate: ");

    //%lf - pole x struktury jest typu double

    //&(p->x) - zapis oznacza adres składowej x
    //w strukturze wskazywanej przez wskaźnik p
    // p      - wskaźnik na strukturę typu point
    // p->x   - pole x w strukturze wskazywanej przez p
    // (operator strzałka pozwala na dostęp do pól
    // wskazywanej struktury
    //&(p->x) - adres pola x

    scanf("%lf",&(p->x));

    printf("y coordinate: ");
    scanf("%lf",&(p->y));
}

void printPoint(struct point* p){
    printf("[%f,%f]",p->x,p->y);
}








