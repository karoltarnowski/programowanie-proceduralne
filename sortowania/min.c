/*Funkcja z zad. 2*/
/*Znajduje element minimalny w tablicy.
Funkcja działa dla tablicy n-elementowej (n>0)*/
/*Funkcja działa analogicznie do funkcji max()
z pliku max.c*/

float min(float t[], int n){
    int i;
    float m = t[0];
    for(i=1; i<n; i++){
        if(t[i] < m)
            m = t[i];
    }
    return m;
}
