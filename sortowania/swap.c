/*Funkcja z zad. 6*/
/*Funkcja pobiera tablicę
oraz dwie pozycje elementów,
które powinny być zamienione miejscami.*/

void swap(float t[], int i, int j){
    float temp;   //zmienna pomocnicza
    temp = t[i];  //wartość t[i] "odkładamy" do zmiennej pomocniczej
    t[i] = t[j];  //wartość t[j] przepisujemy do t[i]
    t[j] = temp;  //wartość t[j] aktualizujemy wartością zmiennej pomocniczej
}
