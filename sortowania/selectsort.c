/*Funkcja z zad. 7*/
/*Funkcja pobiera tablicę oraz jej długość.
Funkcja sortuje elementy rosnąco algorytmem selectsort.*/

#include "sort.h"  /*dołączenie pliku nagłówkowy,
ponieważ wykorzystywane będą funkcje min_i() oraz swap().*/

void selectsort(float t[], int n){
    int min_ind;   //pozycja elementu minimalnego w rozważanym fragmencie tablicy
    int i;         //licznik

    /*n-1 razy znajdowana jest pozycja elementu najmniejszy w nieposortowanym
    fragmencie tablicy, a następnie jest on przestawiany na początek tego fragmentu*/
    for(i=0; i<n-1; i++){
        min_ind = min_i(t+i, n-i); /*w i-tej iteracji
        nieposortowany fragment zaczyna się na pozycji i-tej i obejmuje n-i elementów
        min_ind zawiera pozycję liczoną od i-tego elementu*/
        swap(t+i,min_ind,0);/*zamienione są miejscami element min_ind oraz zerowy we
        fragmencie tablicy zaczynającym się od i-tego elementu*/
    }
}
