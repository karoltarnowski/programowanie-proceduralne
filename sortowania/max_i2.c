/*Funkcja z zad. 3*/
/*Znajduje pozycję i wartość elementu maksymalnego w tablicy.
Funkcja działa dla tablicy n-elementowej (n>0)*/


float max_i2(float t[], int n, int* max_ind){
    int i;                      //licznik pętli
    *max_ind = 0;               //pozycja kandydata na maksimum
    //wykorzystano zmienną przekazaną przez wskaźnik
    float max = t[0];           //wartość kandydata wpisana do zmiennej max
    for(i=1; i<n; i++){         //dla pozostałych elmentów
        if(t[i] > max){         //jeśli element jest większy od kandydata
            max = t[i];         //zaktualizuj wartość
            *max_ind = i;       //zaktualizuj pozycję
        }
    }
    return max;
}
