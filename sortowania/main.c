/*Projekt zawiera rozwiązania zadań 1-9
z laboratorium 5-6 z kursu Programowanie proceduralne*/

/*Zad. 1. Implementacja funkcji znajduje się
w pliku max.c, deklaracja funkcji w pliku sort.h*/

/*Zad. 2. Implementacje funkcji znajdują się w plikach
max_i.c, min.c, min_i.c*/

/*Zad. 3 Implementacja funkcji znajduje się w pliku max_i2.c*/

/*Zad. 6 Implementacja funkcji znajduje się w pliku swap.c*/

/*Zad. 7 Implementacja funkcji znajduje się w pliku selectsort.c*/

/*Zad. 8 Implementacja funkcji znajduje się w pliku bubblesort.c*/

/*Zad. 9 Implementacja funkcji znajduje się w pliku mergesort.c*/

#include <stdio.h>
#include <stdlib.h>
#include "sort.h"
#define N 6

int main()
{
    int max_ind;
    int i;

    //tablica z danymi do demonstracji działania funkcji*/
    float tab[N] = {2, 4, -2.3, 4, -3.2, 1};

    //Funkcja max() (zad.1) znajduje element maksymalny w tablicy
    printf("max = %f\n", max(tab, N) );

    //Funkcja max_i() (zad.2) znajduje pozycję elementu maksymalnego w tablicy
    printf("na pozycji %d\n",max_i(tab,N));
    //Funkcja min() (zad.2) znajduje element minimalny w tablicy
    printf("min = %f\n", min(tab, N) );
    //Funkcja min_i() (zad.2) znajduje pozcyję elementu minimalnego w tablicy
    printf("na pozycji %d\n",min_i(tab,N));

    /*Funkcja max_i2() pobiera trzy argumenty
    - tablicę,
    - rozmiar,
    - pozycję elementu maksymalnego (przez wskaźnik).
    Funkcja zwraca wartość elementu maksymalnego.
    Po takim wywołaniu wartość elementu maksymalnego zapisywana jest w zmiennej m,
    a jego pozycja w zmiennej max_ind.*/
    float m = max_i2(tab, N, &max_ind);
    printf("max = %f\nna pozycji %d\n", m , max_ind);

    //wyświetlenie tablicy
    for(i=0; i<N; i++)
        printf("%f ",tab[i]);
    printf("\n");
    //zamiana miejscami elementów funkcją swap() z pliku swap.c
    swap(tab,0,1);
    //sprawdzenie, że elementy zostały zamienione
    for(i=0; i<N; i++)
        printf("%f ",tab[i]);
    printf("\n");

    /*Testsowanie funkcji sortujących:
    - selectsort(), zad. 7,
    - bubblesort(), zad. 8,
    - mergesort(), zad. 9.*/
    //selectsort(tab,N);
    //bubblesort(tab,N);
    mergesort(tab,N);

    //sprawdzenie, że elementy zostały posortowane
    for(i=0; i<N; i++)
        printf("%f ",tab[i]);
    printf("\n");
}
