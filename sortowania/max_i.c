/*Funkcja z zad. 2*/
/*Znajduje pozycję elementu maksymalnego w tablicy.
Funkcja działa dla tablicy n-elementowej (n>0)*/

int max_i(float t[], int n){
    int i;                  //indeks do przeglądania tablicy
    int im = 0;             //pozycja kandydata - zaczynamy od zerowego
    for(i=1; i<n; i++){     //przeglądane są wszystkie elementy (poza zerowym)
        if(t[i] > t[im])    //jeżeli i-ty element jest większy niż kandydat z pozycji im
            im = i;         //to zmień pozycję kandydata
    }
    return im;
}
