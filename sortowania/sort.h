#ifndef SORT_H_INCLUDED
#define SORT_H_INCLUDED

float max(float[], int);
float min(float[], int);
int max_i(float[], int);
int min_i(float[], int);

float max_i2(float[], int, int*);

void swap(float[], int, int);

void selectsort(float[], int);
void bubblesort(float[], int);

void mergesort(float[], int);
void merge(float[], int, int);

#endif // SORT_H_INCLUDED
