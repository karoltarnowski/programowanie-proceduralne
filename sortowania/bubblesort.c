/*Funkcja z zad. 8*/
/*Funkcja pobiera tablicę oraz jej długość.
Funkcja sortuje elementy rosnąco algorytmem bubblesort.*/

#include "sort.h" /*dołączenie pliku nagłówkowy,
ponieważ wykorzystywana będzie funkcja swap().*/

void bubblesort(float t[], int n){
    int i, j;

    for(i=0; i<n-1; i++)         //iteracje po tablicy
        for(j=0; j<n-1-i; j++)   //iteracje po sasiednich elementach
            if( t[j] > t[j+1] )  //jesli sa w zlej kolejności
                swap(t,j,j+1);   //zamien je miejscami

}
