/*Funkcja z zad. 2*/
/*Znajduje pozycję elementu minimalnego w tablicy.
Funkcja działa dla tablicy n-elementowej (n>0)*/
/*Funkcja działa analogicznie do funkcji max_i()
z pliku max_i.c*/

int min_i(float t[], int n){
    int i;
    int im = 0;
    for(i=1; i<n; i++){
        if(t[i] < t[im])
            im = i;
    }
    return im;
}
