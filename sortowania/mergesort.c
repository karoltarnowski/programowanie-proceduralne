/*Funkcje z zad. 9*/
/*Funkcja mergesort() pobiera tablicę oraz jej długość
i sortuje elementy rosnąco algorytmem mergesort.
Wykorzystywana jest pomocnicza funkcja merge().*/

#include <stdlib.h>  //Do obsługi dynamicznej alokacji pamięci (malloc(), free()).
#include "sort.h"    //Deklaracja funkcji merge().

void mergesort(float t[], int n){
    if( n>1 ){                 //Sortowane są tylko fragmenty tablicy o długości > 1.
                               //Tablice jednoelementowe są posortowane.
        int m = n/2;           //Obliczamy liczbę elementów w lewej cześci tablicy.
        mergesort(t,m);        //Sortujemy rekurencyjnie lewy (m-elementowy) fragment tablicy.
        mergesort(t+m,n-m);    //Sortujemy rekurencyjnie prawy fragment tablicy.
                               //Zaczyna się na od pozycji m, a jego długość to n-m.
        merge(t,m,n);          //Scalanie posortowanych fragmentów.
    }
}

/*Funkcja scala dwa posortowane fragmenty tablicy t.
- lewy  ma nl elementów
- prawy ma nr = n - nl elementów.
|0|1|2|...|nl-2|nl-1| |nl|nl+1|...|n-2|n-1|
      lewy                    prawy
*/
void merge(float t[], int nl, int n){
    int nr = n - nl;
    int i, j, k;

    float * left  = malloc(nl*sizeof(float));  //dynamiczna alokacja pomocniczych tablic
    float * right = malloc(nr*sizeof(float));

    //przepisywanie elementów z oryginalnej tablicy t do tablicy left
    for(i=0; i<nl; i++)
        left[i] = t[i];

    //przepisywanie elementów z oryginalnej tablicy t do tablicy right
    for(j=0; j<nr; j++)
        right[j] = t[nl + j];

    /*wykorzystano trzy indeksy:
    - i (obsługuje tablicę left),
    - j (obsługuje tablicę right),
    - k (obsługuje tablicę t).*/
    i=j=k=0;
    /*Dopóki w obu tablicach zostały jakieś elementy do przejrzenia*/
    while( i<nl && j<nr ){
        if(left[i] < right[j]){ //porównywane są elementy z dwóch tablic - do tablicy t trafia mniejszy
            t[k] = left[i++];   //przy przepisywaniu elementu z left zwiększamy indeks i
        }
        else{
            t[k] = right[j++];  //przy przepisywaniu elementu z right zwiększamy indeks j
        }
        k++; //po przepisaniu elementu zwiększamy indeks k
    }

    //Jeśli opuściliśmy pętlę to elementy do przepisania zostały tylko w jednej tablic pomocniczej

    //Przepisujemy zatem elementy z tablicy left (jeśli nie było tam elementu to nic nie przepiszemy).
    while( i < nl )
        t[k++] = left[i++];

    //Przepisujemy ewentualnie elementy z tablicy right (jeśli nie było tam elementu to nic nie przepiszemy).
    while( j < nr )
        t[k++] = right[j++];

    //Zwalniamy dynamicznie alokowaną pamięć.
    free(right);
    free(left);
}
