/*Funkcja z zad. 1*/
/*Znajduje element maksymalny w tablicy.
Funkcja działa dla tablicy n-elementowej
(n>0)*/

float max(float t[], int n){
    int i;
    float m = t[0];      //zmienna m przechowuje kandydata na max
    for(i=1; i<n; i++){  //przeglądane są wszystkie elementy (poza zerowym)
        if(t[i] > m)     //jeżeli i-ty element jest większy niż kandydat
            m = t[i];    //to zastąp kandydata tym elementem
    }
    return m;
}
