/*funkcja06.c*/
/*Program prosi użytkownika o podanie temperatury
  w stopniach Celsjusza.
  Następnie przelicza podaną temperaturę
  na stopnie Fahrenheita i wyświetla wynik.

  Obliczenia wykonywane są w funkcji float cels2fahr(float).
  Czytanie danych w funkcji readCels().
  Wyświetlenie wyników w funkcji printResult().*/

#include<stdio.h>

//deklaracja funkcji
float cels2fahr(float);
float readCels();
void printResult(float, float);

int main(){
   //deklaracja zmiennej
   float celsjusz;

   //wczytanie danych wejściowych
   celsjusz = readCels();

   //wykonanie obliczeń i wyświetlenie wyniku
   printResult(celsjusz, cels2fahr(celsjusz));

   return 0;
}

//definicje funkcji

float cels2fahr(float c){
   return 32 + 1.8*c;
}

float readCels(){
   float c;
   printf("Podaj temperature w Celsjuszach: ");
   scanf("%f",&c);
   return c;
}

void printResult(float c, float f){
   printf("Temperatura %.2f stopni Celsjusza\n",c);
   printf("odpowiada temperaturze %.2f stopni Fahrenheita.\n", f);
   return;
}
