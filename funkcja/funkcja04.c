/*funkcja04.c*/
/*Program prosi użytkownika o podanie temperatury
  w stopniach Celsjusza.
  Następnie przelicza podaną temperaturę
  na stopnie Fahrenheita i wyświetla wynik.

  Obliczenia wykonywane są w funkcji float cels2fahr(float).
  Czytanie danych w funkcji readCels().*/

#include<stdio.h>

//deklaracje funkcji
float cels2fahr(float);
float readCels();

int main(){
    float celsjusz, fahrenheit;

    //wczytanie danych wejściowych (temperatury w stopniach Celsjusza)
    //z wykorzystaniem funkcji readCels()
    celsjusz = readCels();

    //wykonanie obliczeń
    fahrenheit = cels2fahr(celsjusz);

    //wyświetlenie wyniku
    printf("Temperatura %.2f stopni Celsjusza\n",celsjusz);
    printf("odpowiada temperaturze %.2f stopni Fahrenheita.\n", fahrenheit);

    return 0;
}

//definicja funkcji
float cels2fahr(float c){
    return 32 + 1.8*c;
}

//funkcja wczytywania danych
//funkcja nie pobiera argumentów (pusta lista argumentów)
//funkcja zwraca liczbę - wczytaną temperaturę - zatem
//zwracany typ to float
float readCels(){
    //deklaracja zmiennej lokalnej c
    float c;
    printf("Podaj temperature w Celsjuszach: ");
    //dane są wczytywane do zmiennej lokalnej
    scanf("%f",&c);
    //a następnie jej wartość jest zwracana przez funkcję
    return c;
}
