/*funkcja01.c*/
/*Program prosi użytkownika o podanie temperatury
  w stopniach Celsjusza.
  Następnie przelicza podaną temperaturę
  na stopnie Fahrenheita i wyświetla wynik.*/

#include<stdio.h>

main(){
    float celsjusz, fahrenheit;

    printf("Podaj temperature w Celsjuszach: ");
    scanf("%f",&celsjusz);

    fahrenheit = 32 + 1.8*celsjusz;

    printf("Temperatura %.2f stopni Celsjusza\n",celsjusz);
    printf("odpowiada temperaturze %.2f stopni Fahrenheita.\n", fahrenheit);

    return 0;
}
