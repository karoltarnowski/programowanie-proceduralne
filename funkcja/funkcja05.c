/*funkcja05.c*/
/*Program prosi użytkownika o podanie temperatury
  w stopniach Celsjusza.
  Następnie przelicza podaną temperaturę
  na stopnie Fahrenheita i wyświetla wynik.

  Obliczenia wykonywane są w funkcji float cels2fahr(float).
  Czytanie danych w funkcji readCels().
  Wyświetlenie wyników w funkcji printResult().*/

#include<stdio.h>

//deklaracje funkcji
float cels2fahr(float);
float readCels();
void printResult(float, float);

int main(){
    float celsjusz, fahrenheit;

    //wczytanie danych wejściowych
    celsjusz = readCels();

    //wykonanie obliczeń
    fahrenheit = cels2fahr(celsjusz);

    //wyświetlenie wyniku
    printResult(celsjusz, fahrenheit);

    return 0;
}

//definicje funkcji

float cels2fahr(float c){
   return 32 + 1.8*c;
}

float readCels(){
   float c;
   printf("Podaj temperature w Celsjuszach: ");
   scanf("%f",&c);
   return c;
}

//funkcja wyświetlająca wyniki
//funkcja pobiera dwa argumenty - dwie temperatury - lista argumentów: float c, float f
//funkcja nic nie zwraca - drukuje wyniki na ekranie - zatem zwracany typ to void
void printResult(float c, float f){
   printf("Temperatura %.2f stopni Celsjusza\n",c);
   printf("odpowiada temperaturze %.2f stopni Fahrenheita.\n", f);
   return;
}
