/*funkcja02.c*/
/*Program prosi użytkownika o podanie temperatury
  w stopniach Celsjusza.
  Następnie przelicza podaną temperaturę
  na stopnie Fahrenheita i wyświetla wynik.

  W kodzie wyróżniono poszczególne części funkcjonalne:
    - deklaracja zmiennych,
    - wczytanie danych wejściowych,
    - wykonanie obliczeń,
    - wyświetlenie wyniku.*/

#include<stdio.h>

main(){
    //deklaracja zmiennych
    float celsjusz, fahrenheit;

    //wczytanie danych wejściowych
    printf("Podaj temperature w Celsjuszach: ");
    scanf("%f",&celsjusz);

    //wykonanie obliczeń
    fahrenheit = 32 + 1.8*celsjusz;

    //wyświetlenie wyniku
    printf("Temperatura %.2f stopni Celsjusza\n",celsjusz);
    printf("odpowiada temperaturze %.2f stopni Fahrenheita.\n", fahrenheit);

    return 0;
}
