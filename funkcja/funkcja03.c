/*funkcja03.c*/
/*Program prosi użytkownika o podanie temperatury
  w stopniach Celsjusza.
  Następnie przelicza podaną temperaturę
  na stopnie Fahrenheita i wyświetla wynik.

  Obliczenia wykonywane są w funkcji float cels2fahr(float).*/

#include<stdio.h>

//deklaracja funkcji określa:
//typ zwracanej wartości      - float
//nazwę funkcji               - cels2fahr
//typy pobieranych argumentów - float (w tym przypadku jeden argument).
float cels2fahr(float);

main(){
    float celsjusz, fahrenheit;

    //wczytanie danych wejściowych
    printf("Podaj temperature w Celsjuszach: ");
    scanf("%f",&celsjusz);

    //wykonanie obliczeń
    fahrenheit = cels2fahr(celsjusz); //wywołanie funkcji cels2fahr()
    //funkcja pobiera jako argument wartość zmiennej celsjusz
    //zwrócony wynik jest przypisany do zminnej fahrenheit

    //wyświetlenie wyniku
    printf("Temperatura %.2f stopni Celsjusza\n",celsjusz);
    printf("odpowiada temperaturze %.2f stopni Fahrenheita.\n", fahrenheit);

    return 0;
}

//definicja funkcji
//typ zwracanej wartości              - float
//nazwę funkcji                       - cels2fahr
//typy i nazwy pobieranych argumentów - float c (zmienna automatyczna c typu float).
float cels2fahr(float c){
    //ciało funkcji, w tym przypadku instrukcja return zwracająca wartość wyrażenia
    return 32 + 1.8*c;
}
